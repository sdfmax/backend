from app import db
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String(255), index=True, unique=False)
    country = db.Column(db.String(255), index=True, unique=False)
    street = db.Column(db.String(255), index=True, unique=False)
    events = db.relationship('Event', backref='address', lazy='dynamic')

    def __init__(self, city, country, street):
        self.city = city
        self.country = country
        self.street = street


class AddressSchema(ModelSchema):
    class Meta:
        exclude = ['events']
        model = Address
