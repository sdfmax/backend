from app import db
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields


class DistanceCategory(db.Model):
    __tablename__ = "distance_category"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True)
    events = db.relationship('Event', backref='distance_category',
                             lazy='dynamic')

    def __init__(self, name):
        self.name = name

    def get_or_create_distancecategory(name):
        category = DistanceCategory.query.filter(DistanceCategory.name == name).first()
        if category is None:
            category = DistanceCategory(name)
        return category


class DistanceCategorySchema(ModelSchema):
    class Meta:
        exclude = ['events']
        model = DistanceCategory
