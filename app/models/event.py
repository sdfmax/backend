from app import db
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True, nullable=False)
    desc = db.Column(db.String(1025), nullable=False)
    url_source = db.Column(db.String(255), index=True, unique=True)
    date = db.Column(db.DateTime)
    id_category = db.Column(db.Integer, db.ForeignKey("category.id"))
    id_address = db.Column(db.Integer, db.ForeignKey("address.id"))
    id_federation = db.Column(db.Integer, db.ForeignKey("federation.id"))
    id_distance_category = db.Column(db.Integer,
                                     db.ForeignKey("distance_category.id"))

    def __repr__(self):
        return '<Event %r>' % (self.name)

    def __init__(self, name, desc, date, url_source):
        self.name = name
        self.desc = desc
        self.date = date
        self.url_source = url_source

    def addCategory(self, category):
        self.category = category

    def addAddress(self, address):
        self.address = address

    def addFederation(self, federation):
        self.federation = federation

    def addDistanceCategory(self, distancecategory):
        self.distance_category = distancecategory


class EventSchema(ModelSchema):
    class Meta:
        model = Event
    category = fields.Nested("CategorySchema")
    address = fields.Nested("AddressSchema")
    federation = fields.Nested("FederationSchema")
    distance_category = fields.Nested("DistanceCategorySchema")
