from app import db
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields
from . import category_category
from pprint import pprint


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True)
    desc = db.Column(db.String(1025))
    url_source = db.Column(db.String(255), index=True, unique=True)
    events = db.relationship('Event', backref='category', lazy='dynamic')
    parent = db.relationship('Category', secondary='category_category',
                             primaryjoin="Category.id==category_category.c.id_category_child",
                             secondaryjoin="Category.id==category_category.c.id_category_parent",
                             back_populates='children')
    children = db.relationship('Category', secondary='category_category',
                               primaryjoin="Category.id==category_category.c.id_category_parent",
                               secondaryjoin="Category.id==category_category.c.id_category_child",
                               back_populates='parent')

    def __init__(self, name, desc, url_source):
        self.name = name
        self.desc = desc
        self.url_source = url_source


    def get_or_create_category(name, desc, url=None):
        category = Category.query.filter(Category.url_source == url).first()
        if category is None:
            category = Category(name, desc, url)
        return category


class CategorySchema(ModelSchema):
    class Meta:
        model = Category

    parent = fields.Nested("self", only=['id', 'name', 'desc'], many=True)
    children = fields.Nested("self", only=['id', 'name', 'desc'], many=True)
    exclude = ['events']
