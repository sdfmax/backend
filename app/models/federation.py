from app import db
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields


class Federation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True)
    events = db.relationship('Event', backref='federation', lazy='dynamic')

    def __init__(self, name):
        self.name = name

    def get_or_create_federation(name):
        federation = Federation.query.filter(Federation.name == name).first()
        if federation is None:
            federation = Federation(name)
        return federation


class FederationSchema(ModelSchema):
    class Meta:
        exclude = ['events']
        model = Federation
