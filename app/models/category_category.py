from app import db

category_category = db.Table('category_category',
                             db.Column('id_category_parent', db.Integer, db.ForeignKey('category.id'), primary_key=True),
                             db.Column('id_category_child', db.Integer, db.ForeignKey('category.id'), primary_key=True))
