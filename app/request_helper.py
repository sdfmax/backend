from http_request_randomizer.requests.proxy.requestProxy import RequestProxy
import logging

def RequestHelper(url):
    logger = logging.getLogger('reqhelper')
    req_proxy = RequestProxy(web_proxy_list=[])
    while True:
        try:
            request = req_proxy.generate_proxied_request(url, req_timeout=5)
            if request is not None and request.status_code is 200:
                if request.text.find("UA-58602-34") == -1:
                    pass
                else:
                    logger.debug("Success request for url :" + url)
                    return(request.text)
        except Exception as e:
            logger.debug("Exception raised : " + e.message)
