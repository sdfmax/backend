# coding: utf-8
from app import app, db
from flask import jsonify, abort, request
from app.models.event import EventSchema, Event
from app.models.category import Category
from app.models.federation import Federation
from app.models.address import Address
from app.models.distance_category import DistanceCategory
import locale
import datetime


@app.route('/event/', methods=['GET'])
def events():
    events_schema = EventSchema(many=True)
    all_events = Event.query.all()
    return jsonify({"events": events_schema.dump(all_events).data})


@app.route('/event/<int:id>', methods=['GET'])
def event_id(id):
    event_id = EventSchema()
    event = Event.query.get(id)
    if event is None:
        abort(404)
    return jsonify({"event": event_id.dump(event).data})


@app.route('/event/', methods=['POST'])
def create_event():
    if 'name' not in request.json:
        abort(400)
    event_schema = EventSchema(many=False)
    name = request.json.get('name')
    desc = request.json.get('desc')
    date_raw = request.json.get('date')
    if len(date_raw) == 8:
        date = datetime.datetime.strptime(date_raw, "%Y%m%d")
    else:
        try:
            date = datetime.datetime.strptime(date_raw, "%Y%m%dT%H%M%SZ")
        except ValueError:
            date = datetime.datetime.strptime(date_raw, "%Y%m%dТ%H%M%SZ")
    category = request.json.get('category')
    distance_cat = request.json.get('distance_cat')
    cat_desc = request.json.get('cat_desc')
    cat_href = request.json.get('cat_href')
    country = request.json.get('country')
    city = request.json.get('city')
    street = request.json.get('street')
    federation = request.json.get('federation')
    url_source = request.json.get('url_source')
    event = Event(name, desc, date, url_source)
    address = Address(city, country, street)
    event.addCategory(Category.get_or_create_category(category, cat_desc, cat_href))
    event.addAddress(address)
    event.addFederation(Federation.get_or_create_federation(federation))
    event.addDistanceCategory(DistanceCategory.get_or_create_distancecategory(distance_cat))
    # CHECK DUPLICATE
    db.session.add(event)
    db.session.commit()
    return jsonify({'event': event_schema.dump(event).data}), 201


@app.route('/event/<int:id>', methods=['PUT'])
def update_event(id):
    event_schema = EventSchema(many=False)
    event = Event.query.get(id)
    if event is None:
        abort(404)
    event.name = request.json.get('name', event.name)
    event.desc = request.json.get('desc', event.desc)
    db.session.commit()
    return jsonify({'event': event_schema.dump(event).data})


@app.route('/event/<int:id>', methods=['DELETE'])
def delete_event(id):
    event = Event.query.get(id)
    if event is None:
        abort(404)
    db.session.delete(event)
    db.session.commit()
    return jsonify({"result": True})
