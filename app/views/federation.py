from app import app, db
from flask import jsonify, abort, request
from app.models.federation import FederationSchema, Federation
from pprint import pprint

@app.route('/federation/', methods=['GET'])
def federations():
    federations_schema = FederationSchema(many=True)
    all_federations = Federation.query.all()
    return jsonify({"federations:": federations_schema.dump(all_federations).data})


@app.route('/federation/<int:id>', methods=['GET'])
def federation_id(id):
    federation_id = FederationSchema()
    federation = Federation.query.get(id)
    if federation is None:
        abort(404)
    return jsonify({"federation": federation_id.dump(federation).data})


@app.route('/federation/', methods=['POST'])
def create_federation():
    if 'name' not in request.json:
        abort(400)
    federation_schema = FederationSchema(many=False)
    federation = Federation(request.json.get('name', ''))
    db.session.add(federation)
    db.session.commit()
    return jsonify({'federation': federation_schema.dump(federation).data}), 201


@app.route('/federation/<int:id>', methods=['PUT'])
def update_federation(id):
    federation_schema = FederationSchema(many=False)
    federation = Federation.query.get(id)
    if federation is None:
        abort(404)
    federation.name = request.json.get('name', federation.name)
    db.session.commit()
    return jsonify({'federation': federation_schema.dump(federation).data})


@app.route('/federation/<int:id>', methods=['DELETE'])
def delete_federation(id):
    federation = Federation.query.get(id)
    if federation is None:
        abort(404)
    db.session.delete(federation)
    db.session.commit()
    return jsonify({"result": True})


@app.route('/federation/test', methods=['GET'])
def test():
    check = "FFdeTest3"
    federation = Federation.query.all()
    id_result = -1
    for results in federation:
        if results.name == check:
            id_result = results.id
    federation_schema_result = FederationSchema(many=False)
    if id_result == -1:
        federation_result = Federation(check)
        db.session.add(federation_result)
        db.session.commit()
    else:
        federation_result = Federation.query.get(id_result)
    return jsonify(federation_schema_result.dump(federation_result).data)
