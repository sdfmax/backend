from app import app
import requests
from bs4 import BeautifulSoup
from flask import jsonify, request
from app.models.event import EventSchema, Event
from app.request_helper import RequestHelper
from config import SERVER_URL, SCRAPPER_DEEP
import logging


@app.route('/get_new_events_url', methods=['GET'])
def get_new_events_url():
    base_url = "http://www.calendrier.dusportif.fr"
    url_cat = request.args.get('url')
    urls_pages = []
    urls = []
    deep = SCRAPPER_DEEP
    i = 0
    urls_pages.append(base_url + url_cat)
    while i < deep:
        urls_pages.append(base_url + url_cat + "-p" + str(i + 1))
        i = i + 1
    for url_page in urls_pages:
        html = RequestHelper(url_page)
        soup = BeautifulSoup(html, "lxml")
        a = soup.find_all("tr", class_="vevent")
        for items in a:
            for url in items.find_all("a", {"class": "nbl url uid"}, href=True):
                urls.append(base_url + url['href'])
    new_events = []
    for url in urls:
        if Event.query.filter(Event.url_source == url).first() is None:
            new_events.append(url)
        else:
            pass
    # Boucle de requetes GET sur scrap event pour ajouter les nouveaux events
    logger = logging.getLogger('scrapper')
    logger.debug("found " + str(len(new_events)) + " new events in " + url_cat + ", scraping...")
    for url in new_events:
        requests.get(SERVER_URL + "/scrap_event_from_url" + "?url=" + url)
    return(jsonify(new_events))
