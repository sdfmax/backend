from app import app, db
from flask import jsonify, abort, request
from app.models.distance_category import DistanceCategorySchema, DistanceCategory


@app.route('/distance_category/', methods=['GET'])
def distance_categories():
    distance_categories_schema = DistanceCategorySchema(many=True)
    all_distance_categories = DistanceCategory.query.all()
    return jsonify({'distance_categories': distance_categories_schema.dump(all_distance_categories).data})


@app.route("/distance_category/<int:id>", methods=['GET'])
def distance_category(id):
    distance_category_schema = DistanceCategorySchema(many=False)
    distance_category = DistanceCategory.query.get(id)
    if distance_category is None:
        abort(404)
    return jsonify({'distance_category': distance_category_schema.dump(distance_category).data})


@app.route('/distance_category/', methods=['POST'])
def create_distance_category():
    if 'name' not in request.json:
        abort(400)
    distance_category_schema = DistanceCategorySchema(many=False)
    cat = DistanceCategory(request.json.get('name', ''))
    db.session.add(cat)
    db.session.commit()
    return jsonify({'distance_category': distance_category_schema.dump(cat).data}), 201


@app.route('/distance_category/<int:id>', methods=['PUT'])
def update_distance_category(id):
    distance_category_schema = DistanceCategorySchema(many=False)
    distance_category = DistanceCategory.query.get(id)
    if distance_category is None:
        abort(404)
    distance_category.name = request.json.get('name', distance_category.name)
    db.session.commit()
    return jsonify({'distance_category': distance_category_schema.dump(distance_category).data})


@app.route('/distance_category/<int:id>', methods=['DELETE'])
def delete_distance_category(id):
    distance_category = DistanceCategory.query.get(id)
    if distance_category is None:
        abort(404)
    db.session.delete(distance_category)
    db.session.commit()
    return jsonify({"result": True})
