from app import app, db
from flask import jsonify, abort, request
from app.models.category import CategorySchema, Category


@app.route('/category/', methods=['GET'])
def categories():
    categories_schema = CategorySchema(many=True)
    all_categories = Category.query.all()
    return jsonify({'categories': categories_schema.dump(all_categories).data})


@app.route("/category/<int:id>", methods=['GET'])
def category(id):
    category_schema = CategorySchema(many=False)
    category = Category.query.get(id)
    if category is None:
        abort(404)
    return jsonify({'category': category_schema.dump(category).data})


@app.route('/category/', methods=['POST'])
def create_category():
    if 'name' not in request.json:
        abort(400)
    category_schema = CategorySchema(many=False)
    cat = Category(request.json.get('name', ''), request.json.get('desc', ''))
    db.session.add(cat)
    db.session.commit()
    return jsonify({'category': category_schema.dump(cat).data}), 201


@app.route('/category/<int:id>', methods=['PUT'])
def update_category(id):
    category_schema = CategorySchema(many=False)
    category = Category.query.get(id)
    if category is None:
        abort(404)
    category.name = request.json.get('name', category.name)
    category.desc = request.json.get('desc', category.desc)
    db.session.commit()
    return jsonify({'category': category_schema.dump(category).data})


@app.route('/category/<int:id>', methods=['DELETE'])
def delete_category(id):
    category = Category.query.get(id)
    if category is None:
        abort(404)
    db.session.delete(category)
    db.session.commit()
    return jsonify({"result": True})
