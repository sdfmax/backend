# coding: utf-8
from app import app
import requests
from bs4 import BeautifulSoup
from flask import jsonify, request
import json
import logging
from sqlalchemy import exc
from app.request_helper import RequestHelper
from config import SERVER_URL

@app.route('/scrap_event_from_url', methods=['GET'])
def scrap_event_from_url():
    logger = logging.getLogger('scrapper')
    url = request.args.get('url')
    html = RequestHelper(url)
    soup = BeautifulSoup(html, "lxml")

    try:
        street = soup.find_all("span", {"class": "street-address"})[0].string
    except IndexError:
        street = None

    try:
        for categories in soup.find_all("td", {"class": "category breadcrumb"}):
            categories = categories.find_all('a')
        data = {"name": soup.find_all("span", {"class": "summary"})[0].get_text(),
                "desc": soup.find_all("td", {"class": "asummary description"})[0].get_text(),
                "date": soup.find_all("abbr", {"class": "dtstart"})[0]['title'],
                "category": categories[0].string,
                "distance_cat": categories[1].string,
                "cat_desc": soup.find_all("div", {"class": "quotes"})[0].string,
                "cat_href": categories[0]['href'],
                "country": soup.find_all("span", {"class": "country-name"})[0].string,
                "city": soup.find_all("a", {"class": "locality target"})[0].string,
                "street": street,
                "federation": soup.find("td", text="Fédérаtіоn аffіnіtаіrе").find_next_sibling("td").text,
                "url_source": url}
    except IndexError:
        logger.debug("scrap failed for : " + url)
    client = app.test_client()
    try:
        client.post(SERVER_URL + "/event/", data=json.dumps(data), headers={'Content-type': 'application/json'})
    except exc.IntegrityError:
        return "Duplicate entry", 500
    logger.debug("scrap success for : " + url)
    return jsonify(data)
