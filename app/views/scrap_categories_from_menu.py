from app import app, db
from bs4 import BeautifulSoup
from flask import jsonify, request
import json
import requests
from app.models.category import Category, CategorySchema
from app.request_helper import RequestHelper
from config import SERVER_URL
import logging

@app.route('/scrap_categories_from_menu')
def scrap_categories_from_menu():
    url = "http://www.calendrier.dusportif.fr"
    html = RequestHelper(url)
    soup = BeautifulSoup(html, "lxml")
    url_list = []
    name_list = []
    done = False
    a = soup.find_all("ul", {"class": "calendrier_sport"})
    for items in a:
        for li in items.findAll('li'):
            if done is True:
                break
            for b in li.findAll('a'):
                if(b['href'] == "/agenda-divers"):
                    done = True
                if (b['href'] == '#'):
                    pass
                else:
                    url_list.append(b['href'])
                    name_list.append(b.text)
    for urll, name in zip(url_list, name_list):
        if Category.query.filter(Category.url_source == urll).first()is None:
            requests.get(SERVER_URL + "/get_category?url=" + urll + "&" + "name=" + name)
            # requests.get(SERVER_URL + "/get_category/" + urll + "/" + name)
        else:
            pass
    return (jsonify(url_list))


# @app.route('/get_category/<string:url>/<string:name>')
@app.route('/get_category')
def add_category():
    url = request.args.get('url')
    name = request.args.get('name')
    html = RequestHelper("http://www.calendrier.dusportif.fr" + url)
    soup = BeautifulSoup(html, "lxml")
    desc = soup.find_all("div", {"class": "quotes"})[0].string
    category = Category(name, desc, url)
    db.session.add(category)
    db.session.commit()
    return "ok"

@app.route('/update_all')
def update_all():
    logger = logging.getLogger('scrapper')
    logger.debug("UPDATE All categories and event from Database")
    requests.get(SERVER_URL + "/scrap_categories_from_menu")
    categories_schema = CategorySchema(many=True)
    all_categories = Category.query.all()
    for category in all_categories:
        requests.get(SERVER_URL + "/get_new_events_url?url=" + category.url_source)
    logger.debug("UPDATE Finish")
    return "ok"
