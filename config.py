import os
import logging
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'mysql://root:password@127.0.0.1/sdf'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_ECHO = True

SERVER_URL = "http://127.0.0.1:5000"
SCRAPPER_DEEP = 2

logger = logging.getLogger('scrapper')
hdlr = logging.FileHandler('./logs/Scrapper.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

logger = logging.getLogger('reqhelper')
hdlr = logging.FileHandler('./logs/RequestHelper.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)
